export const STATES = {
	LOADING: 'loading',
	ANSWERING: 'answering',
	RESULT: 'result'
}

export const BUTTON_THEMES = {
	default: 'bg-zinc-50 text-zinc-900',
	danger: 'bg-red-400',
	success: 'bg-green-400'
}
